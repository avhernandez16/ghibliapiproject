package com.avh.retrofit

import Data.Model.Film
import Data.Model.Locations
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.avh.retrofit.databinding.ItemBinding

class GhibliFilmsAdapter(private val films: List<Locations>): RecyclerView.Adapter<GhibliFilmsAdapter.ViewHolder>(){
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GhibliFilmsAdapter.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: GhibliFilmsAdapter.ViewHolder, position: Int) {
        val films = films[position]
        with(holder)
        {
            binding.ghibliId.text = position.toString()
            binding.originalTitle.text =films.name

        }

    }

    override fun getItemCount(): Int {
        return films.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val binding = ItemBinding.bind(view)
    }


}