package com.avh.retrofit

import Data.Model.Film
import Data.Model.Locations
import Data.RetrofitClient
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.avh.retrofit.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var ghibliFilmsAdapter: GhibliFilmsAdapter
    private lateinit var glayout: GridLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        glayout = GridLayoutManager(this, 2)

        binding.recycler.apply{
            layoutManager = glayout
        }

        val service = RetrofitClient.makeRetrofitService()

        lifecycleScope.launch {
            val ghibliFilmsList: Film = service.getFilms()
            val films = ghibliFilmsList.locations
            ghibliFilmsAdapter = GhibliFilmsAdapter(films)
            binding.recycler.adapter = ghibliFilmsAdapter
        }

    }
}