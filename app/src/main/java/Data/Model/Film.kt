package Data.Model

data class Film(
    val description: String,
    val director: String,
    val id: String,
    val locations: List<Locations>,
    val original_title: String,
    val original_title_romanised: String,
    val people: List<Any>,
    val producer: String,
    val release_date: String,
    val rt_score: String,
    val running_time: String,
    val species: List<Any>,
    val title: String,
    val url: String,
    val vehicles: List<Any>
)